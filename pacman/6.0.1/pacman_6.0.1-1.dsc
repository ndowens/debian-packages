Format: 3.0 (quilt)
Source: pacman
Binary: arch-pacman
Architecture: any
Version: 6.0.1-1
Maintainer: Nathan Owens <ndowens@artixlinux.org>
Homepage: https://archlinux.org/pacman
Standards-Version: 4.5.1
Build-Depends: debhelper-compat (= 13), meson, pkg-config, libarchive-dev, libcurl4-dev, libcrypto++-dev, libssl-dev
Package-List:
 arch-pacman deb misc optional arch=any
Checksums-Sha1:
 e77637b05740cf0569e56c9e4513ab3bfcb4754d 880036 pacman_6.0.1.orig.tar.xz
 b079e020eaf8289949e48a8244d85eb9b0726dbb 1888 pacman_6.0.1-1.debian.tar.xz
Checksums-Sha256:
 0db61456e56aa49e260e891c0b025be210319e62b15521f29d3e93b00d3bf731 880036 pacman_6.0.1.orig.tar.xz
 c115b81f142d3a0ae5753bcf02f861cbe63105ff78bdb56858b8440bc5ae0f1f 1888 pacman_6.0.1-1.debian.tar.xz
Files:
 f71e6f06867749735960d0c1d199d375 880036 pacman_6.0.1.orig.tar.xz
 a8630f0a5b2aba79bc9f214f912a9f9f 1888 pacman_6.0.1-1.debian.tar.xz
