Format: 3.0 (quilt)
Source: pacman-contrib
Binary: pacman-contrib
Architecture: any
Version: 1.4.0-1
Maintainer: unknown <ndowens@artixlinux.org>
Standards-Version: 4.6.0
Build-Depends: debhelper-compat (= 13), pkg-config, arch-pacman, libarchive-dev, libcrypto++-dev, libssl-dev, libcurl-dev, asciidoc-base
Package-List:
 pacman-contrib deb misc optional arch=any
Checksums-Sha1:
 73912c0962cb3ad835521b846cb3f4f4b7bb9d27 294452 pacman-contrib_1.4.0.orig.tar.xz
 f5c498a6e9d5ed299495164234b9f6a82aceed62 1592 pacman-contrib_1.4.0-1.debian.tar.xz
Checksums-Sha256:
 07da74cff96a6beac9eebb42c4b2bf8fe569c516003080499928203af72ea1e2 294452 pacman-contrib_1.4.0.orig.tar.xz
 68fe8f4d19dbd655fb9be07f3c6e46ba8375d1f3e5a5aa3fd124d57b558e0d2a 1592 pacman-contrib_1.4.0-1.debian.tar.xz
Files:
 d78adc59eddd05b8b450ec7f5d0cc346 294452 pacman-contrib_1.4.0.orig.tar.xz
 7335a4e99bf15346b482c7793275fca2 1592 pacman-contrib_1.4.0-1.debian.tar.xz
