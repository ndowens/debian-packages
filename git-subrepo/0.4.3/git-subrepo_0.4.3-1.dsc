Format: 3.0 (quilt)
Source: git-subrepo
Binary: git-subrepo
Architecture: any
Version: 0.4.3-1
Maintainer: Nathan Owens <ndowens@artixlinux.org>
Homepage: https://github.com/ingydotnet/git-subrepo
Standards-Version: 4.5.1
Build-Depends: debhelper-compat (= 13), git
Package-List:
 git-subrepo deb misc optional arch=any
Checksums-Sha1:
 6dea5c20f1b9f083e07bf5424f3cc79ba6707c8a 124107 git-subrepo_0.4.3.orig.tar.gz
 dd9b15c92477314f854de2986eaeeae8eb5d09f2 2688 git-subrepo_0.4.3-1.debian.tar.xz
Checksums-Sha256:
 d2e3cc58f8ac3d90f6f351ae2f9cc999b133b8581ab7a0f7db4933dec8e62c2a 124107 git-subrepo_0.4.3.orig.tar.gz
 e0f4f4f5db6c7c8ff095d4fbc5c6f3de83ea345886ef3bc5c1ca166cf10facb0 2688 git-subrepo_0.4.3-1.debian.tar.xz
Files:
 5d3b9216ceef2eb3b00db1b7bc064b0f 124107 git-subrepo_0.4.3.orig.tar.gz
 b10fa2251c36fcae279b034d01417841 2688 git-subrepo_0.4.3-1.debian.tar.xz
