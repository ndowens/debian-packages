Format: 3.0 (quilt)
Source: artools
Binary: artools
Architecture: any
Version: 0.26.1-1
Maintainer: Nathan Owens <ndowens@artixlinux.org>
Homepage: https://gitea.artixlinux.org/artix/artools
Standards-Version: 4.5.1
Build-Depends: debhelper-compat (= 13)
Package-List:
 artools deb misc medium arch=any
Checksums-Sha1:
 3488d9b1c0df03c5188d3a674ee7b80c15334a38 50212 artools_0.26.1.orig.tar.gz
 2a75e55fd0479c91dada1e1a5c4d8f4b96d0fcd5 3700 artools_0.26.1-1.debian.tar.xz
Checksums-Sha256:
 11fdef1e63cd0c7a3a9ecdeec718a3353352ff9e76a331b4063eebc72a922c49 50212 artools_0.26.1.orig.tar.gz
 854bcf289703af94eab93689d1f214fa2543cfcd46413ae8d9668ef2d016ec4b 3700 artools_0.26.1-1.debian.tar.xz
Files:
 9a46f2694252377d63db7b559a69acaf 50212 artools_0.26.1.orig.tar.gz
 233443456877973253cc2811323f3cb8 3700 artools_0.26.1-1.debian.tar.xz
